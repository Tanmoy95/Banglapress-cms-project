<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Login</title>
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" href="css/custom.css">
    <link rel="stylesheet" href="css/font-awesome.css">
</head>
<body>
        <div class="container" style="margin-top: 100px;">
            <div class="row">
                <div class="col-md-4 col-md-offset-4">
                    <div class="panel panel-default">
                        <h3>BanglaPress</h3>

                        <h4>Login With</h4><br>
                        <span><i class="fa fa-facebook"></i></span>
                        <span><i class="fa fa-twitter"></i></span>
                        <span><i class="fa fa-google-plus"></i></span>
                        <br><br>
                        <h4>OR</h4><hr>
                        <div class="panel-body">
                            <form action="">
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Enter Your Email...">
                                </div>

                                <div class="form-group">
                                    <input type="password" class="form-control" placeholder="Enter Your Password...">
                                </div>

                                <div class="form-group">
                                    <input type="submit" class="btn btn-success btn-lg btn-block" value="Login">
                                </div>
                            </form>
                        </div>
                        <div class="lock"><i class="fa fa-lock"></i></div>
                        <div class="label">Login Form</div>
                        <div class="label2">Login Form</div>
                    </div>
                </div>
            </div>
        </div>

</body>
</html>